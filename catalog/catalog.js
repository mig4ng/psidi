const express = require("express");
const bodyParser = require('body-parser');
const hal = require('hal')

const CONSTANTS = require('../constans.json');
const PORT = CONSTANTS.CATALOG_PORT;
const SERVER_ROOT = "http://localhost:" + PORT + "/v1";
const PATH_PRODUCTS = `${SERVER_ROOT}/products`;
const PATH_PHOTO_BARCODE = "http://localhost:" + CONSTANTS.BARCODE_PORT + "/v1";

const app = express();
app.use(bodyParser.json());

const AppDAO = require('./db/dao')
const ProductsRepository = require('./db/products_repository')

const dao = new AppDAO('./db/catalog.db')
const productRepo = new ProductsRepository(dao)

function createDB() {
    productRepo.createTable()
}

createDB();

function paginate(collection, str, index, resPerPage, total) {

    const maxIndex = Number(total) - 1
    collection.link('first', `${str}index=0`)
    collection.link('final', `${str}index=${Number(maxIndex) - (Number(maxIndex) % Number(resPerPage))}`)
    if (Number(index) > 0) {
        collection.link('last', `${str}index=${Number(index) - Number(resPerPage)}`)
    }
    if (Number(resPerPage) + Number(index) < maxIndex) {
        collection.link('next', `${str}index=${Number(index) + Number(resPerPage)}`)
    }
}

app.route("/v1/products")
    .get((req, res) => {

        const resPerPage = 5;
        let indexValue;
        let total;

        if (req.query.index == undefined) {
            indexValue = 0
        } else {
            indexValue = req.query.index
        }

        productRepo.getAll(req.query.name, req.query.barcode)
            .then((data) => {

                total = data.length;

                productRepo.getAllPaginated(indexValue, resPerPage, req.query.name, req.query.barcode)
                    .then((products) => {

                        if (products == undefined) {
                            res.status(400).send("Bad Request!")
                        } else if (products.length == 0) {
                            res.status(404).send("No products found!")
                        } else {

                            const productsCollection = new hal.Resource({}, `${PATH_PRODUCTS}`)

                            let str = `${PATH_PRODUCTS}?`

                            if (req.query.name != undefined) {
                                str += `name=${req.query.name}&`
                            }

                            if (req.query.barcode != undefined) {
                                str += `barcode=${req.query.barcode}&`
                            }

                            paginate(productsCollection, str, indexValue, resPerPage, total)
                            productsCollection.link('find', { href: `${PATH_PRODUCTS}/:orderId`, templated: true })

                            let embedProducts = new Array()
                            Promise.all(products.map((product) => {
                                embedProducts.push(new hal.Resource({ product }, `${PATH_PRODUCTS}/${product.id}`))
                            }))
                                .then(() => {
                                    productsCollection.embed('products', embedProducts)
                                    res.status(200).send(productsCollection)
                                })
                        }

                    })
                    .catch((err) => {
                        res.status(500).send('Error: ' + JSON.stringify(err));
                    })

            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot overwrite the entire collection.");
    })
    .post((req, res) => {

        const product = {
            name: req.body.name,
            photoUrls: req.body.photoUrls,
            barcode: req.body.barcode,
            photoBarcodeUrl: `${PATH_PHOTO_BARCODE}/${req.body.barcode}`
        }

        productRepo.insert(product.name, product.photoUrls, product.photoBarcodeUrl, product.barcode)
            .then((newProduct) => {
                let productHype = new hal.Resource({}, `${PATH_PRODUCTS}/${newProduct.id}`)
                res.status(201).send(productHype.toJSON())
            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    });

app.route("/v1/products/:productId")
    .get((req, res) => {

        productRepo.getById(req.params.productId)
            .then((product) => {
                if (product == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (product.length == 0) {
                    res.status(404).send("Catalog not found!")
                } else {
                    let productHype = new hal.Resource({ product }, `${SERVER_ROOT}/${req.params.productId}`)
                    res.status(200).send(productHype.toJSON())
                }
            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .put((req, res) => {
        res.status(405).send("Cannot overwrite the entire collection.");
    })
    .post((req, res) => {
        res.status(405).send("Cannot creat an existing catalog.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    });


app.put("/v1/products/:productId/uploadImage", (req, res) => {
    var data = '';
    req.on('data', function (chunk) {
        data = data + chunk;
    });
    req.on('end', function () {
        photos[req.params.productId] = data;
        res.status(200).set('Location', SERVER_ROOT + "/product/" + req.params.productId + "/photo").send("");
    });
});

app.listen(PORT, () => {
    console.log(`Listening Catalog Service at port ${PORT}`);
});
