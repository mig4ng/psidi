const express = require("express");
const bodyParser = require('body-parser');
const hal = require('hal');
const axios = require('axios');

const CONSTANTS = require('../constans.json');

const PORT = process.argv[2] || 3010;
const SERVER_ROOT = "http://localhost:" + PORT + "/v1";
const SERVER_BACKOFFICE = "http://localhost:" + CONSTANTS.ORDER_PORT + "/v1";
const PATH_ORDERS = `${SERVER_ROOT}/orders`;
const PATH_PRODUCTS = `${SERVER_ROOT}/products`;
let SELF_IN_BACKOFFICE = ''

const app = express();
app.use(bodyParser.json());

const AppDAO = require('./db/dao');

const OrdersRepository = require('./db/orders_repository');
const OrdersProductsRepository = require('./db/orders_products_repository');
const ProductsRepository = require('./db/products_repository');

const dao = new AppDAO(`${__dirname}/db/provider-${PORT}.db`);
const orderRepo = new OrdersRepository(dao);
const orderProductRepo = new OrdersProductsRepository(dao);
const productRepo = new ProductsRepository(dao);

function createDB() {
    productRepo.createTable()
        .then(() => orderRepo.createTable())
        .then(() => orderProductRepo.createTable())
        .catch((err) => {
            console.log('Error: ')
            console.log(JSON.stringify(err))
        })
}

createDB();

axios.post(SERVER_BACKOFFICE + '/providers', {
	path: SERVER_ROOT
}).then(response => {
	SELF_IN_BACKOFFICE = response.data._links.self.href
	heartbeat()
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function heartbeat() {
    axios.put(SELF_IN_BACKOFFICE + '/heartbeat')
}

app.route("/v1/orders")
    .get((req, res) => {

        orderRepo.getAll()
            .then((orders) => {

                if (orders == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (orders.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {

                    const ordersCollection = new hal.Resource({}, `${PATH_ORDERS}`);

                    let embedOrder = new Array()
                    Promise.all(orders.map((order) => {
                        embedOrder.push(new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`))
                    }))
                        .then(() => {
                            ordersCollection.embed('orders', embedOrder)
                            res.status(200).send(ordersCollection.toJSON())
                        })
                }
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot overwrite the entire collection.");
    })
    .post((req, res) => {

        const order = {
            informUrl: req.body.informUrl,
            price: 0,
	    products: req.body.products,
            status: "quoting"
        }

        let newOrderId

        let totalPrice = 0

	let products = []

        Promise.all(order.products.map(async (product) => {
            await productRepo.getByProductId(product.id)
                .then((dbProduct) => {
		    const randomMultiplier = Math.random() * (1 - 0.75) + 0.75;
                    totalPrice += dbProduct.price * product.quantity * randomMultiplier
		    dbProduct.quantity = product.quantity
		    products.push(dbProduct)
                })
        }))
            .then(() => {
		totalPrice = totalPrice.toFixed(2)
                orderRepo.insert(order.informUrl, totalPrice, order.status)
                    .then((newOrder) => {

                        newOrderId = newOrder.id

                        Promise.all(products.map((product) => {
                            return orderProductRepo.insert(newOrderId, product.id, product.quantity)
                        }))

                        let orderHype = new hal.Resource({}, `${PATH_ORDERS}/${newOrderId}`);
                        res.status(201).send(orderHype.toJSON())

                        sleep(500 * getRandomInt(5)).then(() => {

                            axios.post(`${order.informUrl}/quotes`, {
                                price: totalPrice,
                                informUrl: `${PATH_ORDERS}/${newOrderId}`,
                                path: `${SERVER_ROOT}`
                            })
                                .then((response) => console.log(response.data))

                        })
                    })
            })


    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    })

app.route("/v1/orders/:orderId")
    .get((req, res) => {
        orderRepo.getById(req.params.orderId)
            .then((order) => {

                if (order == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (order.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {

                    let orderHype = new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`)
                    res.status(200).send(orderHype.toJSON())

                }
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot update the order.");
    })
    .post((req, res) => {
        res.status(405).send("Cannot create a order with a existing id.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the order.");
    })

app.route("/v1/orders/:orderId/status")
    .get((req, res) => {

        orderRepo.getById(req.params.orderId)
            .then((order) => {

                if (order == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (order.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {

                    let statusHype = new hal.Resource({ status: order.status }, `${PATH_ORDERS}/${order.id}/status`)
                    statusHype.link('order', `${PATH_ORDERS}/${order.id}`)
                    res.status(200).send(statusHype.toJSON())
                }
            })
    })
    .put((req, res) => {

        let baseOrder

        orderRepo.getById(req.params.orderId)
            .then((order) => {

                if (order == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (order.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {

                    baseOrder = order

                    baseOrder.status = req.body.status

                    orderRepo.update(baseOrder)
                        .then(() => {

                            let statusHype = new hal.Resource({ status: baseOrder.status }, `${PATH_ORDERS}/${req.params.orderId}/status`)
                            statusHype.link('order', `${PATH_ORDERS}/${req.params.orderId}`)
                            res.status(200).send(statusHype.toJSON())

                            if (baseOrder.status == "ready_for_pickup") {
                                axios.put(`${baseOrder.informUrl}/status`, {
                                    status: baseOrder.status
                                })
                            }

                        })

                }
            })

    })
    .post((req, res) => {
        res.status(405).send("Cannot create a status.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete a status.");
    })

app.route("/v1/products")
    .get((req, res) => {

        productRepo.getAll()
            .then((products) => {

                if (products == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (products.length == 0) {
                    res.status(404).send("Products Not Found!")
                } else {

                    const productsCollection = new hal.Resource({}, `${PATH_PRODUCTS}`)

                    let embedProducts = new Array()
                    Promise.all(products.map((product) => {
                        embedProducts.push(new hal.Resource({ product }, `${PATH_PRODUCTS}/${product.id}`))
                    }))
                        .then(() => {
                            productsCollection.embed('products', embedProducts)
                            res.status(200).send(productsCollection)
                        })
                }
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot update the entire collection.");
    })
    .post((req, res) => {

        const product = {
            name: req.body.name,
            productId: req.body.productId,
            price: req.body.price,
            materialStrength: req.body.materialStrength,
            warranty: req.body.warranty,
            brand: req.body.brand
        }

        productRepo.insert(product.name, product.productId, product.price, product.materialStrength, product.warranty, product.brand)
            .then((product) => {

                let productHype = new hal.Resource({}, `${PATH_PRODUCTS}/${product.id}`)
                res.status(201).send(productHype.toJSON())
            })

    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    })

app.get("/v1/products/:productId", (req, res) => {
    productRepo.getById(req.params.productId)
        .then((product) => {

            if (product == undefined) {
                res.status(400).send("Bad Request!")
            } else if (product.length == 0) {
                res.status(404).send("Product Not Found!")
            } else {

                let productHype = new hal.Resource({ product }, `${PATH_PRODUCTS}/${product.id}`)
                res.status(200).send(productHype.toJSON())
            }
        })
});


app.get("/v1/health", (req, res) => {
    res.status(200).send("OK")
});

app.listen(PORT, () => {
    console.log(`Listening Provider Service at port ${PORT}`);
});

setInterval(heartbeat, 60000);
