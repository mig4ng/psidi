const CONSTANTS = require('../constans.json');
const PATH_PHOTO_BARCODE = "http://localhost:" + CONSTANTS.BARCODE_PORT + "/v1";

const PORT = process.argv[2] || 3010;

const AppDAO = require('./db/dao')
const ProductsRepository = require('./db/products_repository')

const dao = new AppDAO(`${__dirname}/db/provider-${PORT}.db`);
const productRepo = new ProductsRepository(dao)


createDB();

const products = [
	{
		name: "pencil",
		price: 2,
		productId: 1,
		photoUrls: "https://images.pexels.com/photos/745759/pexels-photo-745759.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000001",
	},
	{
		name: "notebook",
		price: 5,
		productId: 2,
		photoUrls: "https://images.pexels.com/photos/942872/pexels-photo-942872.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000002",
	},
	{
		name: "clips",
		price: 1,
		productId: 3,
		photoUrls: "https://images.pexels.com/photos/404320/pexels-photo-404320.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000003",
	},
	{
		name: "eraser",
		price: 2,
		productId: 4,
		photoUrls: "https://images.pexels.com/photos/35202/eraser-office-supplies-office-office-accessories.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000004",
	},
	{
		name: "staples",
		price: 6,
		productId: 5,
		photoUrls: "https://images.pexels.com/photos/227383/pexels-photo-227383.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000005",
	},
	{
		name: "post-its",
		price: 5,
		productId: 6,
		photoUrls: "https://images.pexels.com/photos/317356/pexels-photo-317356.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000006",
	},
	{
		name: "pencil-box",
		price: 15,
		productId: 7,
		photoUrls: "https://images.pexels.com/photos/2935006/pexels-photo-2935006.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000007",
	},
	{
		name: "office-supplies-bundle",
		price: 100,
		productId: 8,
		photoUrls: "https://images.pexels.com/photos/159644/art-supplies-brushes-rulers-scissors-159644.jpeg?cs=srgb&dl=pencils-in-stainless-steel-bucket-159644.jpg&fm=jpg",
		barcode: "10000000008",
	},
	{
		name: "pen",
		price: 1,
		productId: 9,
		photoUrls: "https://images.pexels.com/photos/1025585/pexels-photo-1025585.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000009",
	},
	{
		name: "good-pen",
		price: 25,
		productId: 10,
		photoUrls: "https://images.pexels.com/photos/261450/pexels-photo-261450.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000010",
	},
	{
		name: "rubber-bands",
		price: 3,
		productId: 11,
		photoUrls: "https://images.pexels.com/photos/39675/rubber-bands-elastic-bands-office-supplies-stationery-39675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000011",
	},
]

console.log("Adding products")
function createDB() {
    productRepo.createTable().then(() => {
	products.forEach(product => productRepo.insert(product.name, product.productId, product.price, "strong", "2 years","brand"))
    })
}
