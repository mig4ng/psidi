class ProductsRepository {

    constructor(dao) {
        this.dao = dao
    }
  
    createTable() {
      const sql = `
        CREATE TABLE IF NOT EXISTS products (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            productId INTEGER,
            price INTEGER,
            materialStrength TEXT,
            warranty TEXT,
            brand TEXT)`
      return this.dao.run(sql)
    }

    insert(name, productId, price, materialStrength, warranty, brand) {
        return this.dao.run(
            `INSERT INTO products (name, productId, price, materialStrength, warranty, brand)
                VALUES (?, ?, ?, ?, ?, ?)`,
            [name, productId, price, materialStrength, warranty, brand]
        )
    }

    update(product) {
        const { id, name, productId, price, materialStrength, warranty, brand } = product
        return this.dao.run(
            `UPDATE products
            SET name = ?,
                productId = ?,
                price = ?,
                materialStrength = ?,
                warranty = ?,
                brand = ?
            WHERE id = ?`,
            [name, productId, price, materialStrength, warranty, brand, id]
        )
      }

    delete(id) {
        return this.dao.run(
            `DELETE FROM products WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM products WHERE id = ?`,
            [id]
        )
    }

    getAll() {
        return this.dao.all(
            `SELECT * FROM products`
        )
    }

    getByProductId(productId) {
        return this.dao.get(
            `SELECT * FROM products WHERE productId = ?`,
            [productId]
        )
    }

}
  
module.exports = ProductsRepository;
