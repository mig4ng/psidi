class OrdersRepository {
    
    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS orders (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            informUrl TEXT,
            price INTEGER,
            status TEXT CHECK( status IN ('declined', 'quoting', 'awaiting_approval', 'preparing', 'ready_for_pickup', 'finished')))`
        return this.dao.run(sql)
    }

    insert(informUrl, price, status) {
        return this.dao.run(
            `INSERT INTO orders (informUrl, price, status)
                VALUES (?, ?, ?)`,
                [informUrl, price, status]
        )
    }

    update(order) {
        const { id, informUrl, price, status} = order
        return this.dao.run(
            `UPDATE orders
                SET informUrl = ?,
                price = ?,
                status = ?
            WHERE id = ?`,
            [informUrl, price, status, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM orders WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM orders WHERE id =?`,
            [id]
        )
    }

    getAll() {
        return this.dao.all(`SELECT * FROM orders`)
    }

}

module.exports = OrdersRepository;
