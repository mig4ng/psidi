const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const exjwt = require('express-jwt');
const guard = require('express-jwt-permissions')({
    permissionsProperty: 'scopes'
});

const CONSTANTS = require('../constans.json');

const app = express();

const PORT = CONSTANTS.AUTHENTICATION_PORT;

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', `http://localhost:${PORT}`);
    res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const validToken = exjwt({
    secret: 'my very secret psidi jwt secret'
});

let users = {
    admin: {
        password: 'adminPW123',
        scopes: [
            'read:invoices',
            'write:invoices',
            'read:yarns',
            'write:yarns'
        ]
    },
};

app.post('/v1/register', (req, res) => {
    const {
        username,
        password
    } = req.body;

    if (users[username]) res.status(409).json({
        sucess: false,
        token: null,
        err: 'Username exists'
    });

    users[username] = {
        password,
        scopes: [
            'read:invoices',
            'write:invoices',
            'read:yarns',
            'write:yarns'
        ]
    }

    res.json({
        sucess: true,
        err: null
    });
});

app.post('/v1/login', (req, res) => {
    const {
        username,
        password
    } = req.body;

    if (!users[username]) res.status(401).json({
        sucess: false,
        token: null,
        err: 'Username or password is incorrect'
    });

    const user = users[username];

    if (users[username].password == password) {
        let token = jwt.sign({
                username: username,
                scopes: user.scopes
            },
            'my very secret psidi jwt secret', {
                expiresIn: 604800
            });
        res.json({
            sucess: true,
            err: null,
            token
        });
    } else {
        res.status(401).json({
            sucess: false,
            token: null,
            err: 'Username or password is incorrect'
        });
    }
});

app.get('/v1/authTest', validToken, (req, res) => {
    res.send('You are authenticated');
});

app.get('/v1/scopesTest', validToken, guard.check(['read:yarns']), (req, res) => {
    res.send('You have the scopes');
});

app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send(err);
    } else {
        next(err);
    }
});

app.listen(PORT, () => {
    // eslint-disable-next-line
    console.log(`Listening Authentication Service at port ${PORT}`);
});
