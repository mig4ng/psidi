class QuotesRepository {
    
    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS quotes (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            price INTEGER,
            orderId INTEGER,
            providerId INTEGER,
            informUrl INTEGER,
            CONSTRAINT quote_fk_orderId FOREIGN KEY (orderId)
                REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT quote_fk_providerId FOREIGN KEY (providerId)
                REFERENCES providers(id) ON UPDATE CASCADE ON DELETE CASCADE)`
        return this.dao.run(sql)
    }

    insert(price, orderId, providerId, informUrl) {
        return this.dao.run(
            `INSERT INTO quotes (price, orderId, providerId, informUrl)
                VALUES (?, ?, ?, ?)`,
                [price, orderId, providerId, informUrl]
        )
    }

    update(provider) {
        const { id, price, orderId, providerId, informUrl} = provider
        return this.dao.run(
            `UPDATE quotes
                SET price = ?,
                orderId = ?,
                providerId = ?,
                informUrl = ?
            WHERE id = ?`,
            [price, orderId, providerId, informUrl, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM quotes WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM quotes WHERE id =?`,
            [id]
        )
    }
    
    getAllByOrder(orderId) {
        return this.dao.all(`SELECT * FROM quotes WHERE quotes.orderId = '${orderId}'`)
    }

    getByIdByOrder(orderId, id) {
        return this.dao.get(`SELECT * FROM quotes WHERE quotes.orderId = '${orderId}' AND quotes.id = '${id}'`)
    }

}

module.exports = QuotesRepository;