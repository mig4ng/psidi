class OrdersRepository {

    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS orders (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            price INTEGER,
            pickupLocation TEXT,
            status TEXT CHECK( status IN ('declined', 'quoting', 'awaiting_approval', 'preparing', 'ready_for_pickup', 'finished')),
            date TEXT,
            userId INTEGER,
            selectedQuote INTEGER,
	    funfact TEXT,
            CONSTRAINT order_fk_userId FOREIGN KEY (userId)
                REFERENCES user(id) ON UPDATE CASCADE ON DELETE CASCADE)`
        return this.dao.run(sql)
    }

    insert(price, pickupLocation, status, date, userId, selectedQuote, funfact) {
	console.log(funfact)
        return this.dao.run(
            `INSERT INTO orders (price, pickupLocation, status, date, userId, selectedQuote, funfact)
                VALUES (?, ?, ?, ?, ?, ?, ?)`,
            [price, pickupLocation, status, date, userId, selectedQuote, funfact]
        )
    }

    update(order) {
        const { id, price, pickupLocation, status, date, userId, selectedQuote } = order
        return this.dao.run(
            `UPDATE orders
                SET price = ?,
                pickupLocation = ?,
                status = ?,
                date = ?,
                userId = ?,
                selectedQuote = ?
            WHERE id = ?`,
            [price, pickupLocation, status, date, userId, selectedQuote, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM orders WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM orders WHERE id =?`,
            [id]
        )
    }

    getAll(status, price) {

        let sql = `SELECT DISTINCT
            orders.id AS id,
            orders.userId AS userId,
            orders.price AS price,
            orders.pickupLocation AS pickupLocation,
            orders.status AS status,
            orders.date as date,
            orders.selectedQuote as selectedQuote,
	    orders.funfact as funfact
        FROM orders 
        INNER JOIN orders_products on orders.id=orders_products.orderId
        INNER JOIN products on products.id=orders_products.productId
        `

        if (status != undefined) {
            sql += `WHERE orders.status = '${status}'\n`
        }
        if (price != undefined) {
            if (status != undefined) {
                sql += `AND orders.price = '${price}'\n`
            } else {
                sql += `WHERE orders.price = '${price}'\n`
            }
        }

        return this.dao.all(sql)
    }

    getAllPaginated(index, resPerPage, status, price) {

        let sql = `SELECT DISTINCT
            orders.id AS id,
            orders.userId AS userId,
            orders.price AS price,
            orders.pickupLocation AS pickupLocation,
            orders.status AS status,
            orders.date as date,
            orders.selectedQuote as selectedQuote,
	    orders.funfact as funfact
            FROM orders 
        INNER JOIN orders_products on orders.id=orders_products.orderId
        INNER Join products on products.id=orders_products.productId
        `

        if (status != undefined) {
            sql += `WHERE orders.status = '${status}'\n`
        }
        if (price != undefined) {
            if (status != undefined) {
                sql += `AND orders.price = '${price}'\n`
            } else {
                sql += `WHERE orders.price = '${price}'\n`
            }
        }

        sql += `LIMIT ${index},${resPerPage}\n`

        return this.dao.all(sql)
    }

    getByUserId(userId, date, productId) {

        let sql = `SELECT DISTINCT
            orders.id AS id,
            orders.userId AS userId,
            orders.pickupLocation AS pickupLocation,
            orders.status AS status,
            orders.date as date,
            orders.selectedQuote as selectedQuote,
	    orders.funfact as funfact
        FROM orders 
        INNER JOIN orders_products on orders.id=orders_products.orderId
        INNER Join products on products.id=orders_products.productId
        WHERE userId = '${userId}'
        `

        if (date != undefined) {
            sql += `AND orders.date = '${date}'\n`
        }
        if (productId != undefined) {
            sql += `AND products.id = '${productId}'`
        }

        return this.dao.all(sql)
       
    }

    getByUserIdPaginated(index, resPerPage, userId, date, productId) {

        let sql = `SELECT DISTINCT
            orders.id AS id,
            orders.userId AS userId,
            orders.pickupLocation AS pickupLocation,
            orders.status AS status,
            orders.date as date,
            orders.selectedQuote as selectedQuote,
	    orders.funfact as funfact
        FROM orders 
        INNER JOIN orders_products on orders.id=orders_products.orderId
        INNER Join products on products.id=orders_products.productId
        WHERE userId = '${userId}'
        `

        if (date != undefined) {
            sql += `AND orders.date = '${date}'\n`
        }
        if (productId != undefined) {
            sql += `AND products.id = '${productId}'`
        }

        sql += `LIMIT ${index},${resPerPage}\n`

        return this.dao.all(sql)
       
    }

}

module.exports = OrdersRepository;
