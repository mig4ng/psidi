class UsersRepository {
    
    constructor(dao) {
        this.dao = dao
    }
  
    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT,
            firstName TEXT,
            lastName TEXT,
            email TEXT)`
        return this.dao.run(sql)
    }

    insert(username, firstName, lastName, email) {
        return this.dao.run(
            `INSERT INTO users (username, firstName, lastName, email) 
                VALUES (?, ?, ?, ?)`,
            [username, firstName, lastName, email]
        )
    }

    update(user) {
        const { id, username, firstName, lastName, email } = user
        return this.dao.run(
            `UPDATE users
                 SET username = ?,
                    fistName = ?,
                    lastName = ?,
                    email = ?
            WHERE id = ?`,
            [username, firstName, lastName, email, id]
        )
    }

    delete(id) {
        return this.dao.run(
          `DELETE FROM users WHERE id = ?`,
          [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM users WHERE id = ?`,
            [id]
        )
    }

    getAll() {
        return this.dao.all(`SELECT * FROM users`)
    }

}
  
module.exports = UsersRepository;