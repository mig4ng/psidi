class ProvidersRepository {
    
    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS providers (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            lastResponse INTEGER,
            path TEXT)`
        return this.dao.run(sql)
    }

    insert(lastResponse, path) {
        return this.dao.run(
            `INSERT INTO providers (lastResponse, path)
                VALUES (?, ?)`,
                [lastResponse, path]
        )
    }

    update(provider) {
        const { id, lastResponse, path} = provider
        return this.dao.run(
            `UPDATE providers
                SET lastResponse = ?,
                path = ?
            WHERE id = ?`,
            [lastResponse, path, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM providers WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM providers WHERE id =?`,
            [id]
        )
    }

    getAll() {
        return this.dao.all(`SELECT * FROM providers`)
    }

    getByPath(path) {
        return this.dao.get(`SELECT * FROM providers WHERE providers.path = '${path}'`)
    }

}

module.exports = ProvidersRepository;
