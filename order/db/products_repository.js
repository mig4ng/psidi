class ProductsRepository {
    
    constructor(dao) {
        this.dao = dao
    }
  
    createTable() {
      const sql = `
        CREATE TABLE IF NOT EXISTS products (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            photoUrls TEXT,
            photoBarcodeUrl TEXT,
            barcode TEXT)`
      return this.dao.run(sql)
    }

    insert(name, photoUrls, photoBarcodeUrl, barcode) {
        return this.dao.run(
            `INSERT INTO products (name, photoUrls, photoBarcodeUrl, barcode)
                VALUES (?, ?, ?, ?)`,
            [name, photoUrls, photoBarcodeUrl, barcode]
        )
    }

    update(product) {
        const { id, name, photoUrls, photoBarcodeUrl, barcode } = product
        return this.dao.run(
            `UPDATE products
                SET name = ?,
                photoUrls = ?,
                photoBarcodeUrl = ?,
                barcode = ?
            WHERE id = ?`,
            [name, photoUrls, photoBarcodeUrl, barcode, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM products WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM products WHERE id = ?`,
            [id]
        )
    }

    getAll(name, barcode) {

        let sql = `SELECT *
            FROM products
            `

        if (name != undefined) {
            sql += `WHERE name = '${name}'\n`
        }

        if (barcode != undefined) {
            if (name != undefined) {
                sql += `AND barcode = '${barcode}'\n`
            } else {
                sql += `WHERE barcode = '${barcode}'\n`
            }

        }

        return this.dao.all(sql)
    }

    getAllPaginated(index, resPerPage, name, barcode) {

        let sql = `SELECT *
            FROM products
            `

        if (name != undefined) {
            sql += `WHERE name = '${name}'\n`
        }

        if (barcode != undefined) {
            if (name != undefined) {
                sql += `AND barcode = '${barcode}'\n`
            } else {
                sql += `WHERE barcode = '${barcode}'\n`
            }

        }

        sql += `LIMIT ${index},${resPerPage}\n`

        return this.dao.all(sql)
    }

    getByOrderId(orderId) {

        const sql = `SELECT
            products.id,
            products.name, 
            products.photoUrls,
            products.photoBarcodeUrl,
            products.barcode
        FROM products
        INNER join orders_products ON products.id = orders_products.productId
        WHERE orders_products.orderId = '${orderId}'`

        return this.dao.all(sql)
    }

  }
  
  module.exports = ProductsRepository;
