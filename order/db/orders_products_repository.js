class OrdersProductsRepository {
    
    constructor(dao) {
        this.dao = dao
    }
  
    createTable() {
      const sql = `
        CREATE TABLE IF NOT EXISTS orders_products (
            orderId INTEGER,
            productId INTEGER,
            quantity INTEGER,
            CONSTRAINT product_provider_fk_orderId FOREIGN KEY (orderId)
                REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE,
            CONSTRAINT product_provider_fk_productId FOREIGN KEY (productId)
                REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE)`
      return this.dao.run(sql)
    }

    insert(orderId, productId, quantity) {
        return this.dao.run(
            `INSERT INTO orders_products (orderId, productId, quantity)
                VALUES (?, ?, ?)`,
            [orderId, productId, quantity]
        )
    }

    update(order_product) {
        const { orderId, productId, quantity} = order_product
        return this.dao.run(
            `UPDATE orders_products
                SET quantity = ?
            WHERE orderId = ? AND productId = ?`,
            [quantity, orderId, productId]
        )
    }

    delete(orderId, productId) {
        return this.dao.run(
            `DELETE FROM orders_products WHERE orderId = ? AND productId = ?`,
            [orderId, productId]
        )
    }

    getById(orderId, productId) {
        return this.dao.get(
            `SELECT * FROM orders_products WHERE orderId = ? AND productId = ?`,
            [orderId, productId]
        )
    }

}
  
module.exports = OrdersProductsRepository;