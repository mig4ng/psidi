const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const hal = require('hal');
const axios = require('axios');

const CONSTANTS = require('../constans.json');
const PORT = CONSTANTS.ORDER_PORT;
const SERVER_ROOT = "http://localhost:" + PORT + "/v1";
const PATH_ORDERS = `${SERVER_ROOT}/orders`;
const PATH_USERS = `${SERVER_ROOT}/users`;
const PATH_PRODUCTS = `${SERVER_ROOT}/products`
const PATH_PROVIDERS = `${SERVER_ROOT}/providers`;
const FINANCIAL_SERVICE_PATH = `http://localhost:3003/v1`;
const PROVIDER_TIMEOUT_MINUTES = 10

const app = express();
app.use(bodyParser.json());

const AppDAO = require('./db/dao');

const UsersRepository = require('./db/users_repository');
const OrdersRepository = require('./db/orders_repository');
const OrdersProductsRepository = require('./db/orders_products_repository');
const ProductsRepository = require('./db/products_repository');
const ProvidersRepository = require('./db/providers_repository');
const QuotesRepository = require('./db/quotes_repository')

const dao = new AppDAO(__dirname + '/db/order.db');
const userRepo = new UsersRepository(dao);
const orderRepo = new OrdersRepository(dao);
const orderProductRepo = new OrdersProductsRepository(dao);
const productRepo = new ProductsRepository(dao);
const providerRepo = new ProvidersRepository(dao);
const quoteRepo = new QuotesRepository(dao);

const orderStatus = ['declined', 'quoting', 'awaiting_approval', 'preparing', 'ready_for_pickup', 'finished']


function createDB() {
  userRepo.createTable()
    .then(() => orderRepo.createTable())
    .then(() => orderProductRepo.createTable())
    .then(() => productRepo.createTable())
    .then(() => providerRepo.createTable())
    .then(() => quoteRepo.createTable())
    .catch((err) => {
      console.log('Error: ')
      console.log(JSON.stringify(err))
    })
}


createDB();


function paginate(collection, str, index, resPerPage, total) {

  const maxIndex = Number(total) - 1
  collection.link('first', `${str}index=0`)
  collection.link('final', `${str}index=${Number(maxIndex) - (Number(maxIndex) % Number(resPerPage))}`)
  if (Number(index) > 0) {
    collection.link('last', `${str}index=${Number(index) - Number(resPerPage)}`)
  }
  if (Number(resPerPage) + Number(index) < maxIndex) {
    collection.link('next', `${str}index=${Number(index) + Number(resPerPage)}`)
  }
}


// ORDERS

app.route("/v1/orders")
  .get((req, res) => {

    const resPerPage = 5;
    let indexValue;
    let total;

    if (req.query.index == undefined) {
      indexValue = 0
    } else {
      indexValue = req.query.index
    }

    orderRepo.getAll(req.query.status, req.query.price)
      .then((data) => {

        total = data.length;

        orderRepo.getAllPaginated(indexValue, resPerPage, req.query.status, req.query.price)
          .then((data) => {

            if (data == undefined) {
              res.status(400).send("Bad Request!")
            } else if (data.length == 0) {
              res.status(404).send("Orders Not Found!")
            } else {

              const ordersCollection = new hal.Resource({}, `${PATH_ORDERS}`);

              let str = `${PATH_ORDERS}?`

              if (req.query.status != undefined) {
                str += `status=${req.query.status}&`
              }

              if (req.query.price != undefined) {
                str += `price=${req.query.status}&`
              }

              paginate(ordersCollection, str, indexValue, resPerPage, total)
              ordersCollection.link('find', `${PATH_ORDERS}/:orderId`)

              Promise.all(data.map(async order => {
                products = await productRepo.getByOrderId(order.id)
                let newEmbOrd = new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`)
                newEmbOrd.embed('products', products.map(product => new hal.Resource({ product }, ``)))
                return newEmbOrd
              })).then(orders => {
                ordersCollection.embed('orders', orders)
                res.status(200).send(ordersCollection.toJSON())
              })

            }
          })
          .catch((err) => {
            res.status(500).send('Error: ' + JSON.stringify(err));
          })

      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })

  })
  .put((req, res) => {
    res.status(405).send("Cannot overwrite the entire collection.");
  })
  .post(async (req, res) => {

    let createdProducts = new Array();
    let orderId;
    const date = new Date()
    const order = {
      products: req.body.products,
      price: 0,
      pickupLocation: req.body.pickupLocation,
      status: "quoting",
      date: date.toISOString().split('T')[0],
      userId: req.body.userId,
      selectedQuote: null,
      funfact: "The fun fact service was unreachable."
    }

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    await axios.get(`http://numbersapi.com/${month}/${day}/date`, { timeout: 500 }).then(response => {
      order.funfact = response.data
    }).catch(error => {
      console.log(error)
    })

    if (!order.products) return res.status(400).end("Bad request")
    if (order.products.length == 0) return res.status(400).end("Bad request")
    let hasError = false

    order.products.map(product => {
    	if (product.quantity <= 0) hasError = true 
    })
    if (hasError) return res.status(400).end("Bad request")

    orderRepo.insert(order.price, order.pickupLocation, order.status, order.date, order.userId, order.selectedQuote, order.funfact)
      .then((data) => {

        orderId = data.id

        req.body.products.map((product) => {

          let p = {
            orderId: data.id,
            productId: product.id,
            quantity: product.quantity,
          }

          createdProducts.push(p);

        })

      })
      .then(() => {
        return Promise.all(createdProducts.map((product) => {
          return orderProductRepo.insert(product.orderId, product.productId, product.quantity)
        }))
      })
      .then(() => {


        let orderHype = new hal.Resource({}, `${PATH_ORDERS}/${orderId}`)
        res.status(201).send(orderHype.toJSON());

        providerRepo.getAll()
          .then((providers) => {
            providers.map((provider) => {
              if (provider.lastResponse) {
                if ((Date.now() - provider.lastResponse) < PROVIDER_TIMEOUT_MINUTES * 60 * 1000) {
                  axios.post(`${provider.path}/orders`, {
                    userId: order.userId,
                    informUrl: `${PATH_ORDERS}/${orderId}`,
                    products: order.products,
                  })
                }
              }
            })
          })
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })

  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the entire collection.");
  });


app.route("/v1/orders/:orderId")
  .get((req, res) => {

    let orderCollection

    orderRepo.getById(req.params.orderId)
      .then((order) => {

        if (order == undefined) {
          res.status(400).send("Bad Request!")
        } else if (order.length == 0) {
          res.status(404).send("Orders Not Found!")
        } else {
          if (!order.selectedQuote) {
            delete order.price
          }
          delete order.selectedQuote

          orderCollection = new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`)

          orderCollection.link(new hal.Link('user', `${PATH_USERS}/${order.userId}`))

          if (order.selectedQuote == null) {
            orderCollection.link(new hal.Link('quotes', `${PATH_ORDERS}/${order.id}/quotes`))
          }

          productRepo.getByOrderId(order.id)
            .then((products) => {

              let embedProducts = new Array()
              products.map((product) => {
                embedProducts.push(new hal.Resource({ product }, ``))
              });
              orderCollection.embed('products', embedProducts)
              res.status(200).send(orderCollection.toJSON())
            })
        }
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot update the order.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot create a order with a existing id.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the order.");
  });

app.route("/v1/orders/:orderId/quotes")
  .get((req, res) => {

    quoteRepo.getAllByOrder(req.params.orderId)
      .then((quotes) => {

        if (quotes == undefined) {
          res.status(400).send("Bad Request!")
        } else if (quotes.length == 0) {
          res.status(404).send("Quotes Not Found!")
        } else {

          const quotesCollection = new hal.Resource({}, `${PATH_ORDERS}/${req.params.orderId}/quotes`)
          quotesCollection.link('selected quote', `${PATH_ORDERS}/${req.params.orderId}/selectedQuote`)
          var embedQuote = new Array()
          Promise.all(quotes.map((quote) => {
            delete quote.providerId
            delete quote.informUrl
            delete quote.orderId
            embedQuote.push(new hal.Resource({ quote }, `${PATH_ORDERS}/${req.params.orderId}/quotes/${quote.id}`))
          }))
            .then(() => {
              quotesCollection.embed('quotes', embedQuote)
              res.status(200).send(quotesCollection.toJSON())
            })
        }
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot update the quote.");
  })
  .post((req, res) => {

    const quote = {
      price: req.body.price,
      orderId: req.params.orderId,
      providerId: null,
      informUrl: req.body.informUrl
    }

    providerRepo.getByPath(req.body.path)
      .then((provider) => {

        quote.providerId = provider.id

        quoteRepo.insert(quote.price, quote.orderId, quote.providerId, quote.informUrl)
          .then((data) => {
            let quoteHype = new hal.Resource({ quote }, `${PATH_ORDERS}/quotes/${data.id}`)
            res.status(201).send(quoteHype.toJSON())
          })

      })

  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the quote.");
  })

app.route("/v1/orders/:orderId/quotes/:quoteId")
  .get((req, res) => {

    quoteRepo.getByIdByOrder(req.params.orderId, req.params.quoteId)
      .then((quote) => {

        if (quote == undefined) {
          res.status(400).send("Bad Request!")
        } else if (quote.length == 0) {
          res.status(404).send("Orders Not Found!")
        } else {
          delete quote.providerId
          delete quote.informUrl
          delete quote.orderId
          let quoteHype = new hal.Resource({ quote }, `${PATH_ORDERS}/${req.params.orderId}/quotes/${quote.id}`)
          res.status(200).send(quoteHype.toJSON())

        }
      })

  })
  .put((req, res) => {
    res.status(405).send("Cannot update the quote.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot create the quote.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the quote.");
  })

app.route("/v1/orders/:orderId/selectedQuote")
  .get((req, res) => {
    res.status(405).send("Cannot GET at this endpoint.");
  })
  .put((req, res) => {

    let baseOrder

    orderRepo.getById(req.params.orderId)
      .then((order) => {

        if (order == undefined) {
          res.status(400).send("Bad Request!")
        } else if (order.length == 0) {
          res.status(404).send("Orders Not Found!")
        } else if (order.selectedQuote != null) {
          res.status(400).send("Bad Request!")
        } else {

          baseOrder = order

          quoteRepo.getById(req.body.selectedQuote)
            .then((quote) => {


              baseOrder.selectedQuote = quote.id
              baseOrder.price = quote.price
              baseOrder.status = "awaiting_approval"

              orderRepo.update(baseOrder)
                .then(() => {

                  let orderHype = new hal.Resource({ baseOrder }, `${PATH_ORDERS}/${req.params.orderId}`)
                  res.status(200).send(orderHype.toJSON())

                  axios.post(`${FINANCIAL_SERVICE_PATH}/orders`, {
                    orderId: baseOrder.id,
                    price: baseOrder.price,
                    userId: baseOrder.userId
                  })

                })
            })

        }

      })
  })
  .post((req, res) => {
    res.status(405).send("Cannot POST at this endpoint.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot DELETE at this endpoint.");
  })


function notify(to, subject, text) {

  let transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "414df21befb274",
      pass: "9a64c4563465d7"
    }
  });

  let message = {
    from: "group2@psidi.com",
    to: to,
    subject: subject,
    text: text
  }

  transport.sendMail(message, (err, info) => {
    if (err) {
      console.log(err)
    } else {
      console.log(info)
    }
  })

}

app.route("/v1/orders/:orderId/status")
  .get((req, res) => {
    orderRepo.getById(req.params.orderId)
      .then((data) => {
        if (data == undefined) {
          res.status(400).send("Bad Request!")
        } else if (data.length == 0) {
          res.status(404).send("Order not found!")
        } else {
          let statusHype = new hal.Resource({ status: data.status }, `${PATH_ORDERS}/${req.params.orderId}/status`)
          statusHype.link('order', `${PATH_ORDERS}/${req.params.orderId}`)
          res.status(200).send(statusHype.toJSON())
        }
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })
  })
  .put((req, res) => {

    let order;

    orderRepo.getById(req.params.orderId)
      .then((data) => {

        if (data == undefined) {
          res.status(400).send("Bad Request!")
        } else if (data.length == 0) {
          res.status(404).send("Orders Not Found!")
        } else {

          order = data;

          if (req.body.status === "approved") {

            order.status = "preparing"



            quoteRepo.getById(order.selectedQuote)
              .then((quote) => {
                console.log(quote)
                console.log()
                axios.put(`${quote.informUrl}/status`, {
                  status: order.status
                })

              })



            userRepo.getById(order.userId)
              .then((userData) => {

                let message = {
                  to: userData.email,
                  subject: "Order number " + order.id + " has been approved.",
                  text: "Your order number " + order.id + " has been approved by the financial department and is now being prepared."
                }

                notify(message.to, message.subject, message.text)

              })

            let message2 = {
              to: "admin@psidi.com",
              subject: "Order number " + order.id + " has been approved.",
              text: "The order number " + order.id + " has been approved by the financial department and is now being prepared."
            }

            notify(message2.to, message2.subject, message2.text)

          } else if (req.body.status === "declined") {

            order.status = req.body.status

            userRepo.getById(order.userId)
              .then((data) => {

                let message = {
                  to: data.email,
                  subject: "Order number " + order.id + " has been declined.",
                  text: "Your order number " + order.id + " has been declined by the financial department."
                }

                notify(message.to, message.subject, message.text)

              })

            let message2 = {
              to: "admin@psidi.com",
              subject: "Order number " + order.id + " has been declined.",
              text: "The order number " + order.id + " has been declined by the financial department."
            }

            notify(message2.to, message2.subject, message2.text)

          } else if (req.body.status === "ready_for_pickup") {

            order.status = req.body.status

            userRepo.getById(order.userId)
              .then((data) => {

                let message = {
                  to: data.email,
                  subject: "Order number " + order.id + " ready to be pickup",
                  text: "Your order number " + order.id + " is ready to be pickup at " + order.pickupLocation
                }

                notify(message.to, message.subject, message.text)

              })

          } else if (req.body.status === "fulfilled") {

            order.status = req.body.status

            let message = {
              to: "admin@psidi.com",
              subject: "Order number " + order.id + " has been fulfilled.",
              text: "The order number " + order.id + " has been fulfiled."
            }

            notify(message.to, message.subject, message.text)

          } else if (req.body.status === "finished") {

            order.status = req.body.status

            let message = {
              to: "admin@psidi.com",
              subject: "Order number " + order.id + " has been picked up.",
              text: "The order number " + order.id + " has been picked up and the process is finished."
            }

            notify(message.to, message.subject, message.text)

          } else if (req.body.status === "awaiting_approval") {

            order.status = req.body.status

          }

          orderRepo.update(order)
            .then(() => {

              let newStatus = order.status

              let statusHype = new hal.Resource({ status: newStatus }, `${PATH_ORDERS}/${req.params.orderId}/status`)
              statusHype.link('order', `${PATH_ORDERS}/${req.params.orderId}`)
              res.status(200).send(statusHype.toJSON())

            })
        }
      })
  })
  .post((req, res) => {
    res.status(405).send("Cannot create a status.")
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete a status.")
  });

// USERS

app.route("/v1/users")
  .get((req, res) => {
    userRepo.getAll()
      .then((users) => {
        if (users == undefined) {
          res.status(400).send("Bad Request!")
        } else if (users.length == 0) {
          res.status(404).send("No users found!")
        } else {

          const usersCollection = new hal.Resource({}, `${PATH_USERS}`);
          let embedUsers = new Array()

          Promise.all(users.map((user) => {
            embedUsers.push(new hal.Resource({ user }, `${PATH_USERS}/${user.id}`))
          }))
            .then(() => {
              usersCollection.embed('users', embedUsers)
              res.status(200).send(usersCollection.toJSON())
            })
        }
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot update user data.");
  })
  .post((req, res) => {

    const user = {
      username: req.body.username,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    }

    userRepo.insert(user.username, user.firstName, user.lastName, user.email)
      .then((newUser) => {
        let usersHype = new hal.Resource({}, `${PATH_USERS}/${newUser.id}`)
        res.status(200).send(usersHype.toJSON());
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })

  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete user data.");
  });

app.route("/v1/users/:userId")
  .get((req, res) => {
    userRepo.getById(req.params.userId)
      .then((user) => {
        if (user == undefined) {
          res.status(400).send("Bad Request!")
        } else if (user.length == 0) {
          res.status(404).send("No User found!")
        } else {
          let userHype = new hal.Resource({ user }, `${PATH_USERS}/${req.params.userId}`)
          res.status(200).send(userHype.toJSON())
        }
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot update user data.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot create user data.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete user data.");
  })

app.route("/v1/users/:userId/orders")
  .get((req, res) => {

    if (req.params.userId != undefined) {

      const resPerPage = 5;
      let indexValue;
      let total;

      if (req.query.index == undefined) {
        indexValue = 0
      } else {
        indexValue = req.query.index
      }

      orderRepo.getByUserId(req.params.userId, req.query.date, req.query.productId)
        .then((data) => {

          total = data.length;

          orderRepo.getByUserIdPaginated(indexValue, resPerPage, req.params.userId, req.query.date, req.query.productId)
            .then((orders) => {

              if (orders == undefined) {
                res.status(400).send("Bad Request!")
              } else if (orders.length == 0) {
                res.status(404).send("No Orders found!")
              } else {

                const userOrdersCollection = new hal.Resource({}, `${PATH_USERS}/${req.params.userId}/orders`)
                userOrdersCollection.link('user', `${PATH_USERS}/${req.params.userId}`)

                let str = `${PATH_ORDERS}?`

                if (req.query.date != undefined) {
                  str += `date=${req.query.date}&`
                }

                if (req.query.productId != undefined) {
                  str += `productId=${req.query.productId}&`
                }

                paginate(userOrdersCollection, str, indexValue, resPerPage, total)

                Promise.all(orders.map(async order => {
                  products = await productRepo.getByOrderId(order.id)
                  let newEmbOrd = new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`)
                  newEmbOrd.embed('products', products.map(product => new hal.Resource({ product }, ``)))
                  return newEmbOrd
                })).then(os => {
                  userOrdersCollection.embed('orders', os)
                  res.status(200).send(userOrdersCollection.toJSON())
                })

              }
            })
            .catch((err) => {
              res.status(500).send('Error: ' + JSON.stringify(err));
            })
        })
        .catch((err) => {
          res.status(500).send('Error: ' + JSON.stringify(err));
        })
    } else {
      res.status(400).send("Bad Request!")
    }
  })
  .put((req, res) => {
    res.status(405).send("Cannot update user orders data.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot create user orders data.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete user orders data.");
  })

// PROVIDERS

app.route("/v1/providers")
  .get((req, res) => {

    providerRepo.getAll()
      .then((providers) => {

        if (providers == undefined) {
          res.status(400).send("Bad Request!")
        } else if (providers.length == 0) {
          res.status(404).send("No users found!")
        } else {

          const providersCollection = new hal.Resource({}, `${PATH_USERS}`);
          let embedProviders = new Array()

          Promise.all(providers.map((provider) => {
            embedProviders.push(new hal.Resource({ provider }, `${PATH_USERS}/${provider.id}`))
          }))
            .then(() => {
              providersCollection.embed('providers', embedProviders)
              res.status(200).send(providersCollection.toJSON())
            })

        }
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot overwrite the entire collection.");
  })
  .post((req, res) => {

    const provider = {
      lastResponse: (new Date).getTime(),
      path: req.body.path
    }

    providerRepo.getByPath(provider.path).then(existingProvider => {
      if (existingProvider) {
        let providerHype = new hal.Resource({}, `${PATH_PROVIDERS}/${existingProvider.id}`)
        res.status(200).send(providerHype)
      } else {
        providerRepo.insert(provider.lastResponse, provider.path)
          .then((data) => {

            let providerHype = new hal.Resource({}, `${PATH_PROVIDERS}/${data.id}`)
            res.status(201).send(providerHype.toJSON())
          })
      }
    })


  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the entire collection.");
  })

app.route("/v1/providers/:providerId")
  .get((req, res) => {
    providerRepo.getById(req.params.providerId)
      .then((provider) => {
        if (provider == undefined) {
          res.status(400).send("Bad Request!")
        } else if (provider.length == 0) {
          res.status(404).send("No User found!")
        } else {
          let providerHype = new hal.Resource({ provider }, `${PATH_PROVIDERS}/${req.params.providerId}`)
          res.status(200).send(providerHype.toJSON())
        }
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot update provider data.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot create provider data.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete provider data.");
  })

app.route("/v1/providers/:providerId/heartbeat")
  .put((req, res) => {
    providerRepo.getById(req.params.providerId).then(provider => {
      if (!provider) res.status(404).send('Provider not found')
      else {
        provider.lastResponse = Date.now()
        providerRepo.update(provider).then(() => {
          res.status(204).send()
        })
      }
    })
  })

// PRODUCTS 

app.route("/v1/products")
  .get((req, res) => {

    const resPerPage = 5;
    let indexValue;
    let total;

    if (req.query.index == undefined) {
      indexValue = 0
    } else {
      indexValue = req.query.index
    }

    productRepo.getAll(req.query.name, req.query.barcode)
      .then((data) => {

        total = data.length;

        productRepo.getAllPaginated(indexValue, resPerPage, req.query.name, req.query.barcode)
          .then((products) => {

            if (products == undefined) {
              res.status(400).send("Bad Request!")
            } else if (products.length == 0) {
              res.status(404).send("No products found!")
            } else {

              const productsCollection = new hal.Resource({}, `${PATH_PRODUCTS}`)

              let str = `${PATH_PRODUCTS}?`

              if (req.query.name != undefined) {
                str += `name=${req.query.name}&`
              }

              if (req.query.barcode != undefined) {
                str += `barcode=${req.query.barcode}&`
              }

              paginate(productsCollection, str, indexValue, resPerPage, total)
              productsCollection.link('find', `${PATH_PRODUCTS}/:orderId`)
              productsCollection.link('quote products', `${PATH_ORDERS}`)

              let embedProducts = new Array()
              Promise.all(products.map((product) => {
                embedProducts.push(new hal.Resource({ product }, `${PATH_PRODUCTS}/${product.id}`))
              }))
                .then(() => {
                  productsCollection.embed('products', embedProducts)
                  res.status(200).send(productsCollection)
                })
            }

          })
          .catch((err) => {
            res.status(500).send('Error: ' + JSON.stringify(err));
          })

      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })
  })
  .put((req, res) => {
    res.status(405).send("Cannot overwrite the entire collection.");
  })
  .post((req, res) => {

    const product = {
      name: req.body.name,
      price: req.body.price,
      photoUrls: req.body.photoUrls,
      barcode: req.body.barcode,
      photoBarcodeUrl: `${PATH_PHOTO_BARCODE}/${req.body.barcode}`
    }

    productRepo.insert(product.name, product.price, product.photoUrls, product.photoBarcodeUrl, product.barcode)
      .then((newProduct) => {
        let productHype = new hal.Resource({}, `${PATH_PRODUCTS}/${newProduct.id}`)
        res.status(201).send(productHype.toJSON())
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })

  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the entire collection.");
  });

app.route("/v1/products/:productId")
  .get((req, res) => {

    productRepo.getById(req.params.productId)
      .then((product) => {
        if (product == undefined) {
          res.status(400).send("Bad Request!")
        } else if (product.length == 0) {
          res.status(404).send("Catalog not found!")
        } else {
          let productHype = new hal.Resource({ product }, `${SERVER_ROOT}/${req.params.productId}`)
          res.status(200).send(productHype.toJSON())
        }
      })
      .catch((err) => {
        res.status(500).send('Error: ' + JSON.stringify(err));
      })

  })
  .put((req, res) => {
    res.status(405).send("Cannot overwrite the entire collection.");
  })
  .post((req, res) => {
    res.status(405).send("Cannot creat an existing catalog.");
  })
  .delete((req, res) => {
    res.status(405).send("Cannot delete the entire collection.");
  });


app.put("/v1/products/:productId/uploadImage", (req, res) => {
  var data = '';
  req.on('data', function (chunk) {
    data = data + chunk;
  });
  req.on('end', function () {
    photos[req.params.productId] = data;
    res.status(200).set('Location', SERVER_ROOT + "/product/" + req.params.productId + "/photo").send("");
  });
});

app.listen(PORT, () => {
  console.log(`Listening Order Service at port ${PORT}`);
});
