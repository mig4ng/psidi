const axios = require('axios');
const AppDAO = require('./db/dao');
const ProvidersRepository = require('./db/providers_repository');
const dao = new AppDAO('./db/order.db');
const providerRepo = new ProvidersRepository(dao);

function checkProviders(){
	console.log("Checking providers")
	providerRepo.getAll()
	  .then((providers) => {
	    providers.map((provider) => {
		    console.log(provider)
	      axios.get(`${provider.path}/health`).then((response) => {
		  console.log(response.status)
		  if (response.status == 200) {
		  	provider.lastResponse = Date.now()
			console.log(provider)
			providerRepo.update(provider)
		  }
	      })
	    })
	  })
}

checkProviders()
setInterval(checkProviders, 60 * 1000);
