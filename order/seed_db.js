const CONSTANTS = require('../constans.json');
const PATH_PHOTO_BARCODE = "http://localhost:" + CONSTANTS.BARCODE_PORT + "/v1";

const AppDAO = require('./db/dao')
const ProductsRepository = require('./db/products_repository')
const ProvidersRepository = require('./db/providers_repository')

const dao = new AppDAO(__dirname + '/db/order.db')
const productRepo = new ProductsRepository(dao)
const providerRepo = new ProvidersRepository(dao)

const products = [
	{
		name: "pencil",
		price: 2,
		photoUrls: "https://images.pexels.com/photos/745759/pexels-photo-745759.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000001",
	},
	{
		name: "notebook",
		price: 5,
		photoUrls: "https://images.pexels.com/photos/942872/pexels-photo-942872.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000002",
	},
	{
		name: "clips",
		price: 1,
		photoUrls: "https://images.pexels.com/photos/404320/pexels-photo-404320.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000003",
	},
	{
		name: "eraser",
		price: 2,
		photoUrls: "https://images.pexels.com/photos/35202/eraser-office-supplies-office-office-accessories.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000004",
	},
	{
		name: "staples",
		price: 6,
		photoUrls: "https://images.pexels.com/photos/227383/pexels-photo-227383.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000005",
	},
	{
		name: "post-its",
		price: 5,
		photoUrls: "https://images.pexels.com/photos/317356/pexels-photo-317356.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000006",
	},
	{
		name: "pencil-box",
		price: 15,
		photoUrls: "https://images.pexels.com/photos/2935006/pexels-photo-2935006.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000007",
	},
	{
		name: "office-supplies-bundle",
		price: 100,
		photoUrls: "https://images.pexels.com/photos/159644/art-supplies-brushes-rulers-scissors-159644.jpeg?cs=srgb&dl=pencils-in-stainless-steel-bucket-159644.jpg&fm=jpg",
		barcode: "10000000008",
	},
	{
		name: "pen",
		price: 1,
		photoUrls: "https://images.pexels.com/photos/1025585/pexels-photo-1025585.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000009",
	},
	{
		name: "good-pen",
		price: 25,
		photoUrls: "https://images.pexels.com/photos/261450/pexels-photo-261450.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000010",
	},
	{
		name: "rubber-bands",
		price: 3,
		photoUrls: "https://images.pexels.com/photos/39675/rubber-bands-elastic-bands-office-supplies-stationery-39675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
		barcode: "10000000011",
	},
]


function createDB() {
    productRepo.createTable().then(() => {
	console.log('Adding products')
	products.forEach(product => productRepo.insert(product.name, product.photoUrls, `${PATH_PHOTO_BARCODE}/${product.barcode}`, product.barcode))
    	providerRepo.createTable().then(() => {
    		console.log('Adding providers')
		providerRepo.insert((new Date).getTime(), 'http://localhost:3011/v1')
		providerRepo.insert((new Date).getTime(), 'http://localhost:3012/v1')
		providerRepo.insert((new Date).getTime(), 'http://localhost:3013/v1')
    	})
    })

}

createDB();
