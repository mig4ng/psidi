class OrdersRepository {

    constructor(dao) {
        this.dao = dao
    }

    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS orders (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            orderId INTEGER,
            price INTEGER,
            status TEXT CHECK( status IN ('declined', 'awaiting_approval', 'approved')),
            financialUserId INTEGER,
            CONSTRAINT orders_provider_fk_financialUserId FOREIGN KEY (financialUserId)
                REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE)`
        return this.dao.run(sql)
    }

    insert(orderId, price, status, financialUserId) {
        return this.dao.run(
            `INSERT INTO orders (orderId, price, status, financialUserId)
                VALUES (?, ?, ?, ?)`,
            [orderId, price, status, financialUserId]
        )
    }

    update(order) {
        const { id, orderId, price, status, financialUserId } = order
        return this.dao.run(
            `UPDATE orders
                SET orderId = ?,
                price = ?,
                status = ?,
                financialUserId = ?
            WHERE id = ?`,
            [orderId, price, status, financialUserId, id]
        )
    }

    delete(id) {
        return this.dao.run(
            `DELETE FROM orders WHERE id = ?`,
            [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM orders WHERE id =?`,
            [id]
        )
    }

    getAll(status, price, rate) {
        let sql = `SELECT
        orders.id,
        orders.orderId,
        orders.price,
        orders.financialUserId,
        orders.status  
        FROM orders
        INNER JOIN users on users.id = orders.financialUserId
        `
        if (status != undefined) {
            sql += `WHERE orders.status = '${status}'\n`
        }
        if (price != undefined) {
            if (status != undefined) {
                sql += `AND orders.price = ´${price}´\n`
            }else{
                sql += `WHERE orders.status = '${price}'\n`
            }
        }
        if (rate != undefined) {
            if (status != undefined || price != undefined) {
                sql += `AND orders.price = ´${rate}´\n`
            }else{
                sql += `WHERE orders.price = ´${rate}´\n`
            }
        }

        return this.dao.all(sql)
    }

    getAllPaginated(index, resPerPage, status, price, rate) {

        let sql = `SELECT
        orders.id,
        orders.orderId,
        orders.price,
        orders.financialUserId,
        orders.status  
        FROM orders
        INNER JOIN users on users.id = orders.financialUserId
        `
        if (status != undefined) {
            sql += `WHERE orders.status = '${status}'\n`
        }
        if (price != undefined) {
            if (status != undefined) {
                sql += `AND orders.price = ´${price}´\n`
            }else{
                sql += `WHERE orders.status = '${price}'\n`
            }
        }
        if (rate != undefined) {
            if (status != undefined || price != undefined) {
                sql += `AND orders.price = ´${rate}´\n`
            }else{
                sql += `WHERE orders.price = ´${rate}´\n`
            }
        }

        sql += `LIMIT ${index},${resPerPage}\n`

        return this.dao.all(sql)
    }

}

module.exports = OrdersRepository;