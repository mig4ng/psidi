class UsersRepository {
    
    constructor(dao) {
        this.dao = dao
    }
  
    createTable() {
        const sql = `
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            userId INTEGER,
            rate TEXT CHECK( rate IN ('A', 'B', 'C')))`
        return this.dao.run(sql)
    }

    insert(userId, rate) {
        return this.dao.run(
            `INSERT INTO users (userId, rate) 
                VALUES (?, ?)`,
            [userId, rate]
        )
    }

    update(user) {
        const { id, userId, rate } = user
        return this.dao.run(
            `UPDATE users
                 SET userId = ?,
                    rate = ?
            WHERE id = ?`,
            [userId, rate, id]
        )
    }

    delete(id) {
        return this.dao.run(
          `DELETE FROM users WHERE id = ?`,
          [id]
        )
    }

    getById(id) {
        return this.dao.get(
            `SELECT * FROM users WHERE id = ?`,
            [id]
        )
    }

    getAll() {
        return this.dao.all(`SELECT * FROM users`)
    }

    async getByUserId(userId) {
        let userGot = await this.dao.get(`SELECT * FROM users WHERE userId = '${userId}'`)
        if (!userGot) {
            let newUserId = await this.insert(userId, "A")
            return this.getById(newUserId.id)
        }
        return userGot


    }

}
  
module.exports = UsersRepository;