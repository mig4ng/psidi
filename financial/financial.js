const express = require('express');
const bodyParser = require('body-parser');
const hal = require('hal');
const axios = require('axios');

const CONSTANTS = require('../constans.json');
const PORT = CONSTANTS.FINANCIAL_PORT;
const SERVER_ROOT = "http://localhost:" + PORT + "/v1";
const PATH_ORDERS = `${SERVER_ROOT}/orders`;
const PATH_USERS = `${SERVER_ROOT}/users`;
const ORDER_SERVICE_PATH = `http://localhost:3001/v1`;

const app = express();
app.use(bodyParser.json());

const AppDAO = require('./db/dao');

const UsersRepository = require('./db/users_repository');
const OrdersRepository = require('./db/orders_repository');

const dao = new AppDAO(__dirname + '/db/financial.db');
const userRepo = new UsersRepository(dao);
const orderRepo = new OrdersRepository(dao);

function createDB() {
    userRepo.createTable()
        .then(() => orderRepo.createTable())
}

createDB();

function paginate(collection, str, index, resPerPage, total) {

    const maxIndex = Number(total) - 1
    collection.link('first', `${str}index=0`)
    collection.link('final', `${str}index=${Number(maxIndex) - (Number(maxIndex) % Number(resPerPage))}`)
    if (Number(index) > 0) {
        collection.link('last', `${str}index=${Number(index) - Number(resPerPage)}`)
    }
    if (Number(resPerPage) + Number(index) < maxIndex) {
        collection.link('next', `${str}index=${Number(index) + Number(resPerPage)}`)
    }
}

app.route("/v1/orders")
    .get((req, res) => {

        const resPerPage = 5;
        let indexValue;
        let total;

        if (req.query.index == undefined) {
            indexValue = 0
        } else {
            indexValue = req.query.index
        }

        orderRepo.getAll(req.query.status, req.query.price, req.query.rate)
            .then((data) => {

                total = data.length;

                orderRepo.getAllPaginated(indexValue, resPerPage, req.query.status, req.query.price, req.query.rate)
                    .then((orders) => {

                        if (orders == undefined) {
                            res.status(400).send("Bad Request!")
                        } else if (orders.length == 0) {
                            res.status(404).send("Orders Not Found!")
                        } else {

                            const ordersCollection = new hal.Resource({}, `${PATH_ORDERS}`);

                            let str = `${PATH_ORDERS}?`

                            if (req.query.status != undefined) {
                                str += `status=${req.query.status}&`
                            }

                            if (req.query.price != undefined) {
                                str += `price=${req.query.status}&`
                            }

                            if (req.query.rate != undefined) {
                                str += `rate=${req.query.rate}&`
                            }

                            paginate(ordersCollection, str, indexValue, resPerPage, total)
                            ordersCollection.link('find', { href: `${PATH_ORDERS}/:orderId`, templated: true })

                            let embedOrders = new Array()
                            Promise.all(data.map((order) => {
                                embedOrders.push(new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`))
                            }))
                                .then(() => {
                                    ordersCollection.embed('orders', embedOrders)
                                    res.status(200).send(ordersCollection.toJSON())
                                })
                                .catch((err) => {
                                    res.status(500).send('Error: ' + JSON.stringify(err));
                                })
                        }
                    })
                    .catch((err) => {
                        res.status(500).send('Error: ' + JSON.stringify(err));
                    })
            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .put((req, res) => {
        res.status(405).send("Cannot overwrite the entire collection.");
    })
    .post((req, res) => {

        const order = {
            orderId: req.body.orderId,
            price: req.body.price,
            status: "awaiting_approval",
            financialUserId: null
        }

        let finUser;

        userRepo.getByUserId(req.body.userId)
            .then((user) => {

                finUser = user

                order.financialUserId = user.userId

                if ((order.price < 1000 && finUser.rate != 'C') || (order.price < 5000 && (finUser.rate == 'A' || finUser.rate == 'B'))) {
                    order.status = "approved"
                }

                orderRepo.insert(order.orderId, order.price, order.status, order.financialUserId)
                    .then((data) => {
                        let orderHype = new hal.Resource({}, `${PATH_ORDERS}/${data.id}`)
                        orderHype.link('user', `${PATH_USERS}/${order.financialUserId}`)
                        res.status(201).send(orderHype.toJSON());

                        if (order.status == "approved") {

                            axios.put(`${ORDER_SERVICE_PATH}/orders/${order.orderId}/status`, {
                                status: order.status
                            })

                        }
                    })
                    .catch((err) => {
                        res.status(500).send('Error: ' + JSON.stringify(err));
                    })
            })


    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    })

app.route("/v1/orders/:orderId")
    .get((req, res) => {

        orderRepo.getById(req.params.orderId)
            .then((order) => {
                let orderHype = new hal.Resource({ order }, `${PATH_ORDERS}/${order.id}`)
                orderHype.link(new hal.Link('user', `${PATH_USERS}/${order.financialUserId}`))
                res.status(200).send(orderHype.toJSON())
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot update the given order.");
    })
    .post((req, res) => {
        res.status(405).send("Cannot create an existing order.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the given order.");
    })

app.route("/v1/orders/:orderId/approval")
    .get((req, res) => {
        res.status(405).send("Cannot GET at this endpoint.");
    })
    .put((req, res) => {

        let baseOrder;

        orderRepo.getById(req.params.orderId)
            .then((order) => {

                if (order == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (order.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {

                    baseOrder = order;

                    if (req.body.status === "approved") {

                        baseOrder.status = req.body.status

                    } else if (req.body.status === "declined") {

                        baseOrder.status = req.body.status

                    }


                    orderRepo.update(baseOrder)
                        .then(() => {
                            let statusHype = new hal.Resource({ status: baseOrder.status }, ``)
                            statusHype.link('order', `${PATH_ORDERS}/${req.params.orderId}`)
                            res.status(200).send(statusHype.toJSON())

                            axios.put(`${ORDER_SERVICE_PATH}/orders/${baseOrder.orderId}/status`, {
                                status: baseOrder.status
                            })
                        })
                        .catch((err) => {
                            res.status(500).send('Error: ' + JSON.stringify(err));
                        })
                }

            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .post((req, res) => {
        res.status(405).send("Cannot POST at this endpoint.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot DELETE at this endpoint.");
    })

app.route("/v1/users")
    .get((re, res) => {

        userRepo.getAll()
            .then((users) => {

                if (users == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (users.length == 0) {
                    res.status(404).send("No users found!")
                } else {

                    const usersCollection = new hal.Resource({}, `${PATH_USERS}`)
                    let embedUsers = new Array()

                    Promise.all(users.map((user) => {
                        embedUsers.push(new hal.Resource({ user }, `${PATH_USERS}/${user.id}`))
                    }))
                        .then(() => {
                            usersCollection.embed('users', embedUsers)
                            res.status(200).send(usersCollection.toJSON())
                        })
                }
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot overwrite the entire collection.");
    })
    .post((req, res) => {

        const user = {
            userId: req.body.userId,
            rate: req.body.rate
        }

        userRepo.insert(user.userId, user.rate)
            .then((user) => {
                let userHype = new hal.Resource({}, `${PATH_USERS}/${user.id}`)
                res.status(201).send(userHype.toJSON());
            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the entire collection.");
    })

app.route("/v1/users/:userId")
    .get((req, res) => {

        userRepo.getById(req.params.userId)
            .then((user) => {

                if (user == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (user.length == 0) {
                    res.status(404).send("No User found!")
                } else {

                    let userHype = new hal.Resource({ user }, `${PATH_USERS}/${req.params.userId}`)
                    res.status(200).send(userHype.toJSON())
                }
            })
    })
    .put((req, res) => {
        res.status(405).send("Cannot update the given user.");
    })
    .post((req, res) => {
        res.status(405).send("Cannot create an existing user.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot delete the given user.");
    })

app.route("/v1/users/:userId/rate")
    .get((req, res) => {

        userRepo.getById(req.params.userId)
            .then((user) => {

                if (user == undefined) {
                    res.status(400).send("Bad Request!")
                } else if (user.length == 0) {
                    res.status(404).send("Orders Not Found!")
                } else {
                    let rateHype = new hal.Resource({ rate: user.rate }, `${PATH_ORDERS}/${req.params.userId}/rate`)
                    rateHype.link('user', `${PATH_ORDERS}/${req.params.userId}`)
                    res.status(200).send(rateHype.toJSON())

                }
            })
            .catch((err) => {
                res.status(500).send('Error: ' + JSON.stringify(err));
            })

    })
    .put((req, res) => {
        res.status(405).send("Cannot PUT at this endpoint.");
    })
    .post((req, res) => {
        res.status(405).send("Cannot POST at this endpoint.");
    })
    .delete((req, res) => {
        res.status(405).send("Cannot DELETE at this endpoint.");
    })

app.listen(PORT, () => {
    console.log(`Listening Financial Service at port ${PORT}`);
});
