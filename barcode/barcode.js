const express = require('express')
const app = express()
const JsBarcode = require('jsbarcode')
const { createCanvas } = require("canvas")

const CONSTANTS = require('../constans.json')
const PORT = CONSTANTS.BARCODE_PORT

app.get('/v1/:barcodeId', function (req, res, next) {
	res.setHeader('Content-Type', 'image/png')
	const canvas = createCanvas()
	JsBarcode(canvas, req.params.barcodeId)
	canvas.pngStream().pipe(res)
})

app.listen(PORT, () => {
  console.log(`Listening Barcode Service at port ${PORT}`)
})
