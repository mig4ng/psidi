# PSIDI new

## Assignment #1

### Schemas

    Order(Purchase):
        id:
        userId:
        products: []
        pickupLocation:
        status: ["quoting", "preparing", "ready for pickup", "finished"]
        date:

    Quote:
        id:
        products: []
        providerId:
        price:

    Product:
        id:
        barcode:
        name:
        materialStrength:
        warranty:
        brand:
        photoUrls:

    User(Customer):
        id:
        username:
        password:
        firstName:
        lastName:
        email:

    Provider:
        id:
        products: []
        lastResponse: date
